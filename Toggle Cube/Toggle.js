// Toggle JS
function $_Toggle() {
    var _gettoggle = {};
    this.Set_Toggle = function(param,param2) {
        // param = name & param2 = once
        if (typeof(param) == "string" && typeof(param2) == "boolean" && _gettoggle[param] !== undefined) {
            let f0 = _gettoggle[param];
            f0.once = param2;
        }
    };
    this.Reset_Toggle = function(param) {
        // param = name
        if (typeof(param) == "string" && _gettoggle[param] !== undefined) {
            let f0 = _gettoggle[param];
            f0.value = false;
            f0._once = 0;
        }
    };
    this.Delete_Toggle = function(param) {
        // param = name
        if (typeof(param) == "string" && _gettoggle[param] !== undefined) {
            delete _gettoggle[param];
        }
    };
    this.Copy_Toggle = function(param,param2) {
        // param = name & param2 = name2
        if (typeof(param) == "string" && typeof(param2) == "string" && _gettoggle[param] !== undefined && _gettoggle[param2] === undefined) {
            let f0 = _gettoggle[param];
            let f1 = _gettoggle[param2];
            f1.on = false;
            f1._once = 0;
            f1.once = f0.once;
        }
    };
    this.Add_Toggle = function(param,param2) {
        // param = name & param2 = once
        if (typeof(param) == "string" && _gettoggle[param] === undefined && typeof(param2) == "boolean") {
            _gettoggle[param] = {};
            let f0 = _gettoggle[param];
            f0.on = false;
            f0.once = param2;
            f0._once = 0;
        }
    };
    this.On_Toggle = function(param) {
        // param = name
        if (typeof(param) == "string" && _gettoggle[param] !== undefined) {
            let f0 = _gettoggle[param];
            return f0.on;
        }
    };
    this.Toggle_On = function(param) {
        // param = name
        if (_gettoggle[param] !== undefined && typeof(param) == "string") {
            let f0 = _gettoggle[param];
            if (f0.once === false) {
                if (f0.on === false) {
                    f0.on = true;
                    return;
                } else {
                    f0.on = false;
                    return;
                }
            } else {
                if (f0.on === false && f0._once === 0) {
                    f0.on = true;
                    f0._once = 1;
                    return;
                }
            }
        }
    };
}
var Toggle = new $_Toggle();