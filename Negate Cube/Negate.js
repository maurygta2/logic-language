// Negate function
function $_Negate() {
    var _getnegate = {};
    this.On_Negate = function(param) {
        // param = name
        if (_getnegate[param] !== undefined && typeof(param) == "string") {
            let f1;
            for (let f0 in _getnegate[param]) {
                if (_getnegate[param][f0] !== true &&(f1 === true || f1 === undefined)) {
                    f1 = true;
                } else {
                    f1 = false;
                }
            }
            return f1;
        }
    };
    this.Set_Negate = function(param,param2) {
        // param = name & param2 = value
        if (_getnegate[param] !== undefined && typeof(param) == 'string' && typeof(param2) == "object") {
            for (let f0 in param2) {
                if (typeof(param2[f0]) == "boolean") {
                    _getnegate[param][f0] = param2[f0];
                }
            }
        }
    };
    this.Delete_Negate = function(param) {
         // param = name
         if (_getnegate[param] !== undefined && typeof(param) == "string") {
             delete _getnegate[param];
         }
    };
    this.Copy_Negate = function(param,param2) {
        // param = name & param2 = name2
        if (_getnegate[param] !== undefined && _getnegate[param2] === undefined && typeof(param) == "string" && typeof(param2) == "string") {
            _getnegate[param2] = {};
            let f1 = _getnegate[param];
            let f2 = _getnegate[param2];
            for (let f0 in f1) {
                f2[f0] = f1[f0];
            }
        }
    };
    this.Add_Negate = function(param2,param) {
        // param2 = name & param = value
        if (_getnegate[param2] === undefined && typeof(param2) == "string" && typeof(param) == "object") {
            _getnegate[param2] = {};
            for (let f0 in param) {
                if (typeof(param[f0]) == "boolean") {
                    _getnegate[param2][f0] = param[f0];
                }
            }
        }
    };
}
var Negate = new $_Negate();