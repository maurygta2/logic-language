// And Function
function $_And() {
    this.Add_And = function(param,param2) {
        // param = name & param2 = value
        if (typeof(param) == "string" && typeof(param2) == "object" && _getand[param] === undefined) {
            _getand[param] = {};
            for (let f1 in param2) {
                if (typeof(param2[f1]) == "boolean") {
                    _getand[param][f1] = param2[f1];
                }
            }
        }
    };
    this.Set_And = function(param,param2) {
        // param = name & param2 = value
        if (typeof(param) == "string" && typeof(param2) == 'object' && _getand[param] !== undefined) {
            let f1 = _getand[param];
            for (let f0 in param2) {
                if (typeof(param2[f0]) == "boolean") {
                    f1[f0] = param2[f0];
                }
            }
        }
    };
    this.Copy_And = function(param,param2) {
        // param = name & param2 = name2
        if (typeof(param) == "string" && typeof(param2) == "string" && _getand[param] !== undefined && _getand[param2] === undefined) {
            let f1 = _getand[param];
            _getand[param2] = {};
            let f2 = _getand[param2];
            for (let f0 in f1) {
                f2[f0] = f1[f0];
            }
        }
    };
    var _getand = {};
    this.Delete_And = function(param) {
        // param = name
        if (typeof(param) == "string" && _getand[param] !== undefined) {
            delete _getand[param];
        }
    };
    this.On_And = function(param) {
        // param  = name
        if (typeof(param) == "string" && _getand[param] !== undefined) {
            var f0; // true or false
            var f1 = 0;
            var f5 = _getand[param];
            for (let f3 in f5) {
                if (f5[f3] === true && f1 === 0) {
                    f0 = true;
                } else {
                    f0 = false;
                    f1 = 1;
                }
            }
            return f0;
        }
    };
    this.get = _getand;
}

// example
var And = new $_And();
And.Add_And("a",{
    parame: true,
    k: true
});
console.log(And.On_And("a"))