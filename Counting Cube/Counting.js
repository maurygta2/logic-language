function $_Counting() {
    this.Counting_On = function(param) {
        // param = name
        if (typeof(param) == "string" && _getcounting[param] !== undefined) {
            const f1 = _getcounting[param];
            if (f1.reset === true) {
                if (f1.value > 0) {
                    f1.value -= 1;
                } else {
                    f1.value = f1.limit;
                }
            } else if(f1.value > 0) {
                f1.value -= 1;
            }
        }
    };
    this.On_Counting = function(param) {
        // param = name
        if (typeof(param) == "string" && _getcounting[param] !== undefined) {
            const f1 = _getcounting[param];
            if (f1.value <= 0) {
                return true;
            } else {
                return false;
            }
        }
    };
    this.Delete_Counting = function(param) {
        // param = name
        if (typeof(param) == "string" && _getcounting[param] !== undefined) {
            delete _getcounting[param];
        }
    };
    this.Copy_Counting = function(param,param2) {
        // param = name & param2 = name2
        if (typeof(param) == "string" && typeof(param2) == "string" && _getcounting[param] !== undefined) {
            if (_getcounting[param2] === undefined) {
                _getcounting[param2] = {};
                const f1 = _getcounting[param2];
                const f2 = _getcounting[param];
                f1.value = f2.value_1;
                f1.limit = f2.limit;
                f1.reset = f2.reset;
            }
        }
    };
    this.Reset_Counting = function(param) {
        // param = name
        if (typeof(param) == "string" && _getcounting[param]) {
            const f1 = _getcounting[param];
            f1.value = f1.value_1;
        }
    };
    var _getcounting = {};
    this.Add_Counting = function(param) {
        // param = object value
        if (typeof(param.name) == "string" && typeof(param.value) == "number" && typeof(param.limit) == 'number' && param.value <= param.limit && param.value >= 0 && param.limit >= 0 && typeof(param.reset) == "boolean") {
            if (_getcounting[param.name] === undefined) {
                _getcounting[param.name] = {};
                const f1 = _getcounting[param.name];
                f1.value = param.value;
                f1.value_1 = param.value;
                f1.limit = param.limit;
                f1.reset = param.reset;
            }
        }
    };
}
var Counting = new $_Counting();
