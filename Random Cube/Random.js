// Random Function
function $_Random() {
    var _getrandom = {};
    this.Add_Random = function(param,param2) {
        // param = name & param2 = value
        if (typeof(param) == "string" && typeof(param2) == "number" && param2 > 0 && _getrandom[param] === undefined) {
            _getrandom[param] = {};
            let f0 = _getrandom[param];
            f0.random = param2;
        }
    };
    this.Delete_Random = function(param) {
        // param = name
        if (typeof(param) == "string" && _getrandom[param] !== undefined) {
            delete _getrandom[param];
        }
    };
    this.Set_Random = function(param,param2) {
        // param = name & param2 = value
        if (typeof(param) == "string" && typeof(param2) == "number" && param2 > 0 && _getrandom[param] !== undefined) {
            let f0 = _getrandom[param];
            f0.random = param2;
        }
    };
    this.Copy_Random = function(param,param2) {
        // param = name & param2 = name2
        if (typeof(param) == "string" && typeof(param2) == "string" && _getrandom[param] !== undefined && _getrandom[param2] === undefined) {
            _getrandom[param2] = {};
            let f0 = _getrandom[param];
            let f1 = _getrandom[param2];
            f1.random = f0.random;
        }
    };
    this.On_Random = function(param) {
        // param = name
        if (typeof(param) == "string" && _getrandom[param] !== undefined) {
            let f0 = _getrandom[param];
            let f1 = Math.floor(Math.random()*f0.random+1);
            return f1;
        }
    };
}
var Random = new $_Random();