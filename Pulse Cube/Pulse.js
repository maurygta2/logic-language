// Pulse Function
function $_Pulse() {
    var _getpulse = {};
    var f_2 = setInterval(f_1,100);
    async function f_1() {
        for (let f0 in _getpulse) {
            let f1 = _getpulse[f0];
            let f2 = 0;
            if (f1.call && f1.value == (f1.enable+f1.disable)) {
                f1.value = 0;
            }
            if (f1.call && f1.value < (f1.enable+f1.disable)) {
                f1.value += 1;
                f1.call = false;
                f2 = 1;
            }
            if (!f1.call && f2 === 0) {
                f1.value = 0;
            }
        }
    }
    this.Add_Pulse = function(param,param2,param3) {
        // param = name & param2 = enable & param3 = disable
        if (_getpulse[param] === undefined && typeof(param) == "string" && typeof(param2) == "number" && typeof(param3) == "number" && param2 > 0 && param3 > 0) {
            _getpulse[param] = {};
            let f0 = _getpulse[param];
            f0.value = 0;
            f0.enable = param2;
            f0.disable = param3;
            f0.call = false;
        }
    };
    this.Delete_Pulse = function(param) {
        // param = name
        if (typeof(param) == "string" && _getpulse[param] !== undefined) {
            delete _getpulse[param];
        }
    };
    this.Copy_Pulse = function(param,param2) {
        // param = name & param2 = name2
        if (typeof(param) == "string" && typeof(param2) == "string" && _getpulse[param] !== undefined && _getpulse[param2] === undefined) {
            _getpulse[param2] = {};
            let f0 = _getpulse[param];
            let f1 = _getpulse[param2];
            f1.value = 0;
            f1.enable = f0.enable;
            f1.disable = f0.disable;
            f1.call = false;
        }
    };
    this.Reset_Pulse = function(param) {
        // param = name
        if (_getpulse[param] !== undefined && typeof(param) == "number") {
            let f0 = _getpulse[param];
            f0.value = 0;
            f0.call = false;
        }
    };
    this.Set_Pulse = function(param,param2,param3) {
        // param = name & param2 = enable & param3 = disable
        if (typeof(param) == "string" && _getpulse[param] !== undefined && typeof(paran2) == "number" && param2 > 0 && param3 > 0 && typeof(param3) == "number") {
            let f0 = _getpulse[param];
            f0.value = 0;
            f0.enable = param2;
            f0.disable = param;
            f0.call = false;
        }
    };
    this.Pulse_On = function(param) {
        // param = name
        if (_getpulse[param] !== undefined && typeof(param) == "string") {
            let f0 = _getpulse[param];
            if (f0.value === 0) {
                f0.value += 1;
            }
            f0.call = true;
        }
    };
    this.On_Pulse = function(param) {
        // param = name
        if (typeof(param) == "string" && _getpulse[param] !== undefined) {
            let f0 = _getpulse[param];
            if (f0.value <= f0.enable && f0.call) {
                return true;
            }
            if (f0.value === 0) {
                return false;
            }
            if (f0.value > f0.enable) {
                return false;
            }
        }
    };
}

var Pulse = new $_Pulse();
