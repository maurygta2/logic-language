// Delay function
function $_Delay() {
    var _getdelay = {};
    function __f1() {
        for (let f1 in _getdelay) {
            const f0 = _getdelay[f1];
            if (f0.on === true) {
                if (f0.value < f0.disabled + f0.enabled) {
                    f0.value += 1;
                    console.log(f0.value*100+" ms");
                } else {
                    f0.value = 0;
                    f0.on = false;
                }
            }
        }
    }
    var __f2 = setInterval(__f1,100);
    this.Add_Delay = function(param,param2,param3) {
        // param = name & param2 = disabled & param3 = enabled
        if (_getdelay[param] === undefined && typeof(param) == "string" && typeof(param2) == "number" && typeof(param3) == "number") {
            _getdelay[param] = {};
            const f0 = _getdelay[param];
            f0.disabled = param2;
            f0.enabled = param3;
            f0.value = 0;
            f0.on = false;
        }
    };
    this.Delete_Delay = function(param) {
        // param = name
        if (typeof(param) == "string" && _getdelay[param] !== undefined) {
            delete _getdelay[param];
        }
    };
    this.Copy_Delay = function(param,param2) {
        // param = name1 & param2 = name 2
        if (typeof(param) == "string" && typeof(param2) == "string" && _getdelay[param] !== undefined && _getdelay[param2] === undefined) {
            _getdelay[param2] = {};
            const f0 = _getdelay[param];
            const f1 = _getdelay[param2];
            f1.disabled = f0.disabled;
            f1.enabled = f0.enabled;
            f1.value = 0;
            f1.on = false;
        }
    };
    this.On_Delay = function(param) {
        // param = name
        if (typeof(param) == "string" && _getdelay[param] !== undefined) {
            const f0 = _getdelay[param];
            if (f0.value > f0.disabled) {
                return true;
            } else {
                return false;
            }
        }
    };
    this.Reset_Delay = function(param) {
        // param = name
        if (typeof(param) == "string" && _getdelay[param] !== undefined) {
            const f0 = _getdelay[param];
            f0.value = 0;
            f0.on = false;
        }
    };
    this.Delay_On = function(param) {
        // param = name
        if (_getdelay[param] !== undefined && typeof(param) == "string") {
            const f0 = _getdelay[param];
            if (f0.on === false) {
                f0.on = true;
            }
        }
    };
}
var Delay = new $_Delay();