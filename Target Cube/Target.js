/* L++ by Maury 
Target
*/
function $_Target() {
    var _gettarget = {};
    var f_1 = setInterval(f_2,100);
    async function f_2() {
        for (let f0 in _gettarget) {
            let f1 = _gettarget[f0];
            if (f1.on && !f1.toggle && f1.value < f1.duration) {
                f1.value += 1;
            } else if (!f1.toggle) {
                f1.on = false;
                f1.value = 0;
            }
            if (f1.on && f1.toggle && f1.value < f1.duration) {
                f1.value += 1;
            } else if (f1.toggle) {
                f1.on = false;
            }
        }
    }
    this.Add_Target = function(param,param2,param3) {
        // param = name & param2 = duration & param3 = toggle
        if (_gettarget[param] === undefined && typeof(param) == "string" && typeof(param2) == "number" && param2 > 0 && typeof(param3) == "boolean") {
            _gettarget[param] = {};
            let f0 = _gettarget[param];
            f0.value = 0;
            f0.duration = param2;
            f0.on = false;
            f0.toggle = param3;
        }
    };
    this.Copy_Target = function(param,param2) {
        // param = name & param2 = name2
        if (typeof(param) == "string" && typeof(param2) == "string" && _gettarget[param] !== undefined && _gettarget[param2] === undefined) {
            let f0 = _gettarget[param];
            _gettarget[param2] = {};
            let f1 = _gettarget[param2];
            f1.value = 0;
            f1.duration = f0.duration;
            f1.on = false;
            f1.toggle = f0.toggle;
        }
    };
    this.On_Target = function(param) {
        // param = name
        if (typeof(param) == "string" && _gettarget[param] !== undefined) {
            let f0 = _gettarget[param];
            return f0.on;
        }
    };
    this.Set_Target = function(param,param2,param3) {
        // param = name & param2 = duration & param3 = toggle
        if (_gettarget[param] !== undefined && typeof(param) == "string" && typeof(param2) == "number" && param2 > 0 && typeof(param3) == "boolean") {
            let f0 = _gettarget[param];
            f0.value = 0;
            f0.duration = param2;
            f0.on = false;
            f0.toggle = param3;
        }
    };
    this.Delete_Target = function(param) {
        // param = name
        if (_gettarget[param] !== undefined && typeof(param) == "string") {
            delete _gettarget[param];
        }
    };
    this.Reset_Target = function(param) {
        // param = name
        if (_gettarget[param] !== undefined && typeof(param) == "string") {
            let f0 = _gettarget[param];
            f0.value = 0;
            f0.on = false;
        }
    };
    this.Target_On = function(param) {
        // param = name
        if (_gettarget[param] !== undefined && typeof(param) == "string") {
            let f0 = _gettarget[param];
            f0.on = true;
        }
    };
}
var Target = new $_Target();